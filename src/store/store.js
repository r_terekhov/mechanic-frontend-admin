import { createStore } from "vuex";
import * as constants from "../constants.js";

export default createStore({
  state: {
    url: process.env.VUE_APP_URL,
    requests: [],
    filterType: constants.REQ_IN_WORK,
    user: {
      loggedIn: false,
      idToken: null,
      data: null,
    },
  },
  getters: {
    isAuthenticated(state) {
      return state.user.loggedIn;
    },
    idToken(state) {
      return state.user.idToken;
    },
    user(state) {
      return state.user;
    },
    getRequests(state) {
      return state.requests;
    },
    getFilterType(state) {
      return state.filterType;
    },
  },
  mutations: {
    SET_LOGGED_IN(state, value) {
      state.user.loggedIn = value;
    },
    SET_ID_TOKEN(state, value) {
      state.user.idToken = value;
    },
    SET_USER(state, data) {
      state.user.data = data;
    },
    SET_REQUESTS(state, data) {
      state.requests = data;
    },
    UPDATE_REQUEST(state, changingRequest) {
      console.log("111");
      const request = state.requests.find(
        (request) => request.id === changingRequest.id
      );
      console.log(request);
      Object.assign(request, changingRequest);
    },
    SET_FILTER_TYPE(state, filterType) {
      state.filterType = filterType;
    },
  },
  actions: {
    async fetchUser({ commit }, user) {
      commit("SET_LOGGED_IN", user !== null);
      if (user) {
        user.getIdToken().then((idToken) => {
          commit("SET_ID_TOKEN", idToken);
          commit("SET_USER", {
            uid: user.uid,
            displayName: user.displayName,
            email: user.email,
          });
        });
      } else {
        commit("SET_USER", null);
      }
    },

    async changeRequestStatus(context, payload) {
      const id = payload.id;
      const nextStatus = payload.nextStatus;
      console.log(id, nextStatus);
      if (nextStatus === constants.REQ_IN_WORK) {
        const response = await fetch(
          `${this.state.url}request/inWork?requestId=${id}`,
          {
            method: "POST",
            headers: {
              Authorization: "Bearer " + this.state.user.idToken,
            },
          }
        );

        const responseData = await response.json();
        if (!response.ok) {
          const error = new Error(
            responseData.comment ||
              "failed to update request status with id" + id
          );
          throw error;
        }

        const responseRequest = responseData.response;
        context.commit("UPDATE_REQUEST", responseRequest);
      } else if (nextStatus === constants.REQ_CLOSE) {
        const response = await fetch(
          `${this.state.url}request/close?requestId=${id}&closeComment=closedByAdmin`,
          {
            method: "POST",
            headers: {
              Authorization: "Bearer " + this.state.user.idToken,
            },
          }
        );

        const responseData = await response.json();
        if (!response.ok) {
          const error = new Error(
            responseData.comment ||
              "failed to update request status with id" + id
          );
          throw error;
        }

        const responseRequest = responseData.response;
        context.commit("UPDATE_REQUEST", responseRequest);
      }
    },

    async loadRequests(context) {
      const response = await fetch(`${this.state.url}request/getAll`, {
        headers: {
          method: "GET",
          Authorization: "Bearer " + this.state.user.idToken,
        },
      });

      const responseData = await response.json();
      if (!response.ok) {
        const error = new Error(
          responseData.comment || "failed to get requests"
        );
        context.commit("SET_REQUESTS", []);
        throw error;
      }

      const responseRequests = responseData.response;

      const requests = [];
      responseRequests.forEach((request) => {
        requests.push(request);
      });
      // for (const id in responseRequests) {
      //   const request = {
      //     id: responseRequests[id].id,
      //     status: responseRequests[id].status,
      //     createdTimestamp: responseRequests[id].createdTimestamp,
      //     clientName: responseRequests[id].clientName,
      //     clientPhone: responseRequests[id].clientPhone,
      //     needDiagnostic: responseRequests[id].needDiagnostic,
      //     arrivalMechanicDateTime: responseRequests[id].arrivalMechanicDateTime,
      //     address: responseRequests[id].address,
      //     machine: responseRequests[id].machine,
      //     machineYear: responseRequests[id].machineYear,
      //     problemDescription: responseRequests[id].problemDescription,
      //   };
      //   requests.push(request);
      //     }
      context.commit("SET_REQUESTS", requests);
      // const requests = [];
      // const request = {
      //   id: "123",
      //   created: 1606780800,
      //   clientName: "clientName",
      //   clientPhone: "clientPhone",
      //   needDiagnostic: true,
      //   arrivalMechanicDateTime: 1606780800,
      //   address: "Балтийская 31",
      //   machine: "Range Rover",
      //   machineYear: 2017,
      //   problemDescription: "кошка в дворниках",
      //   status: "OPEN",
      // };
      // requests.push(request);
      // context.commit("SET_REQUESTS", requests);
    },
  },
});
