import { createRouter, createWebHistory } from "vue-router";
import Login from "../components/Login";
import Dashboard from "../components/Dashboard";
import store from "../store/store";

const routes = [
  {
    path: "/",
    name: "dashboard",
    component: Dashboard,
    meta: {
      auth: true,
    },
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  mode: history,
});

router.beforeEach((to, from, next) => {
  const isAuthenticated = store.getters.isAuthenticated;

  const requiresAuth = to.matched.some((record) => record.meta.auth);

  if (requiresAuth && !isAuthenticated) next("/login");
  else if (!requiresAuth && isAuthenticated) next("/");
  else next();
});

export default router;
