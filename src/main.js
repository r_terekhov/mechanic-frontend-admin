import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/router.js";
import store from "./store/store.js";
import firebase from "firebase/app";
import PulseLoader from "vue-spinner/src/PulseLoader.vue";
import BaseDialog from "./components/ui/BaseDialog.vue";
import BaseButton from "./components/ui/BaseButton.vue";

const config = {
  apiKey: "AIzaSyCEaAKzWuz-JZQIWdsdUZpTYFBp_gWS-2o",
  authDomain: "mechanic-testing-37bdf.firebaseapp.com",
  databaseURL: "https://mechanic-testing-37bdf.firebaseio.com",
  projectId: "mechanic-testing-37bdf",
  storageBucket: "mechanic-testing-37bdf.appspot.com",
  messagingSenderId: "785075912677",
  appId: "1:785075912677:web:52d0c6f32055460bbaf90c",
};

firebase.initializeApp(config);

const app = createApp(App);
app.use(store);
app.use(router);
app.component("pulse-loader", PulseLoader);
app.component("base-dialog", BaseDialog);
app.component("base-button", BaseButton);
app.mount("#app");
firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    router.push("/");
  } else {
    router.push("/login");
  }
  store.dispatch("fetchUser", user);
});
